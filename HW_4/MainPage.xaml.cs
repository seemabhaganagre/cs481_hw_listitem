﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace HW_4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        Color bgcolor;
        public MainPage()
        {
            InitializeComponent();
            fillVeggies();

        }

            void fillVeggies()
            {
                VListView.ItemsSource = new List<VList>  //creatting a list object and calling it
            {
                new VList // adding an item
                {
                    Image = "a.jpg",
                    Name = "Avocado",
                    Calories = "calories:238",
                    Origin = "Mexico"
                },
                new VList //adding an item 
                {
                    Image = "b.jpg",
                    Name = "Broccoli",
                    Calories = "Calories:23 ",
                    Origin = "Medditarian"
                },
                new VList // adding an item
                {
                    Image = "beets.jpg",
                    Name = "Beetroot",
                    Calories = "alories: 54",
                    Origin = "Europe"
                },
                new VList //adding an item
                {
                    Image = "bp.jpg",
                    Name = "Bell Peppers",
                    Calories = "Calories: 22",
                    Origin = "Asia"
                },
                new VList //adding an item
                {
                    Image = "bs.jpg",
                    Name = "Brussel Srpouts",
                    Calories = "Calories: 20",
                    Origin = "Medditarian"
                },
                new VList // adding an item
                {
                    Image = "c.jpg",
                    Name = "Cucumber",
                    Calories = "Calories: 25",
                    Origin = "Medditarian"
                },
                new VList //adding an item
                {
                    Image = "ca.jpg",
                    Name = "Carrots",
                    Calories = "Calories: 18",
                    Origin = "Italy"
                },
                new VList //adding an item
                {
                    Image = "p.jpg",
                    Name = "Sweet pea",
                    Calories = "Calroies:36",
                    Origin = "Italy"
                },
                new VList // adding an item
                {
                    Image = "s.jpg",
                    Name = "Spinach",
                    Calories = "Calories:22",
                    Origin = "Persia"
                },
                new VList  //adding an item 
                {
                    Image = "t.jpg",
                    Name = "Tomatoes",
                    Calories = "Calories:20",
                    Origin = "Central Aisa"
                }
            };
            
         }
        

        public MainPage(string name, string cal, string org) //method invoked when a new item is added
        {
            InitializeComponent();
            bgcolor = Color.Black;
        }

        void Handle_Refreshing(object sender, System.EventArgs e) // refresh method
        {
            bgcolor = Color.Azure;
            VListView.IsRefreshing = false;
        }
        async void OnTabView(object sender, Xamarin.Forms.ItemTappedEventArgs e) // long press to add an item, method invoked when long pressed
        {
            var seemaView = (ListView)sender;
            VList LItem = (VList)seemaView.SelectedItem;
            await Navigation.PushAsync(new Page1(LItem));

        }
        async void AddOnClicked(object sender, EventArgs e) //method invoked when add is clicked
        {
           await Navigation.PushAsync(new Page2());
        }


    }
}
