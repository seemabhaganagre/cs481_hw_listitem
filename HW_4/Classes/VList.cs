﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace HW_4
{
   public class VList
    {
    public string Name { get; set; }
    public string Calories { get; set; }
    public string Origin { get; set; }
    public string Image { get; set; }
}
}